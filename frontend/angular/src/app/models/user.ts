export class User {
    _id: number;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    type: string;
    phone: string;
    abo: String;
    token: string;
}