import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../models/user';
import { Subscription } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  currentUser: User;
  currentUserSubscription: Subscription;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService, private userService: UserService) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  getAll() {
    return this.http.get<User[]>("api/users");
  }

  getById(id: number) {
    return this.http.get(`api/users/${id}`);
  }

  register(user: User) {
    return this.http.post(`api/users/register`, user);
  }

  update(user: User) {
    return this.http.put(`api/users/${this.currentUser._id}`, user);
  }

  delete(id: number) {
    return this.http.delete(`api/users/${id}`);
  }

}
