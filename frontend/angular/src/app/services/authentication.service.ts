import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../models/user';



@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject:  BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
   }

   public get currentUserValue(): User {
     return this.currentUserSubject.value;
   }

   login(email: string, password: string) {
     return this.http.post<any>(`api/users/authenticate`, { email, password }).pipe(
       map(user => {
         // login success if jwt token present
         if (user && user.token) {
           localStorage.setItem('currentUser', JSON.stringify(user));
           this.currentUserSubject.next(user);
         }
         console.log(user);
         return user;
       }));
   }

   // update local user data
   updateUser(user) {
     console.log(user);
     localStorage.setItem('currentUser', JSON.stringify(user));
     this.currentUserSubject.next(user);
     this.currentUser = this.currentUserSubject.asObservable();
   }

   logout() {
     // remove user from local storage to log user out
     localStorage.removeItem('currentUser');
     this.currentUserSubject.next(null);
   }
}
