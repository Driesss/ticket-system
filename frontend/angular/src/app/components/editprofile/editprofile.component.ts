import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../services/authentication.service';
import { UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';

import { User } from '../../models/user';

import { first, merge } from 'rxjs/operators';
import { Subscription, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {

  editForm: FormGroup;
  loading = false;
  submitted = false;

  currentUser: User;
  currentUserSubscription: Subscription;

  types: string[] = ["normal user", "organizer"];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService,
    private authenthicationService: AuthenticationService
  ) {
    this.currentUserSubscription = this.authenthicationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      firstName: [this.currentUser.firstName, Validators.required],
      lastName: [this.currentUser.lastName, Validators.required],
      email: [this.currentUser.email, [Validators.required, Validators.email]],
      phone: [this.currentUser.phone, Validators.nullValidator],
      type: [this.currentUser.type, Validators.nullValidator]
    });
  }

  get f() { return this.editForm.controls; }
  
  onSubmit() {
    this.submitted = true;

    if (this.editForm.invalid) {
      return;
    }

    this.loading = true;
    this.userService.update(this.editForm.value).pipe(first()).subscribe(user => {
      
      // get current token
      const token = this.currentUser.token;
      //console.log({...user, token});
      // update current user with new data and current token
      this.authenthicationService.updateUser({
        ...user,
        token
      });
      this.alertService.success('Profile Updated', true);
      this.router.navigate(['/profile']);
    },
    error => {
      this.alertService.error(error);
      this.loading = false;
    })
  }


}
