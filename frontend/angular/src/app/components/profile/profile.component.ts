import { Component, OnInit, OnDestroy } from '@angular/core';

import { User } from '../../models/user';

import { AuthenticationService } from '../../services/authentication.service';
import { UserService } from '../../services/user.service';

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  currentUser: User;
  currentUserSubscription: Subscription;

  constructor(private authenticationService: AuthenticationService, private userService: UserService) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    // unsubscribe to ensure no mem leaks
    this.currentUserSubscription.unsubscribe();
  }

}
