import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './services/authentication.service';

import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currentUser: User;

  constructor(
    private router: Router,
    private authentivationService: AuthenticationService
  ) {
    this.authentivationService.currentUser.subscribe(user => this.currentUser = user);
  }

  logout() {
    this.authentivationService.logout();
    this.router.navigate(['/login']);
  }
}
