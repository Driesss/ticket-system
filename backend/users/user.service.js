const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('helpers/db');
const User = db.User;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function authenticate({ email, password }) {
    console.log(`User: ${email} tried password: ${password}`);
    const user = await User.findOne({ email });
    console.log(user);
    if (user && bcrypt.compareSync(password, user.hash)) {
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id }, config.secret);
        return {
            ...userWithoutHash,
            token
        };
    }
}

async function getAll() {
    return await User.find().select('-hash');
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function create(userParam) {
    // validate
    //email: String = userParam.email;
    email = userParam.email.toLowerCase();
    const userLowerCase = {
        firstName: userParam.firstName.toLowerCase(),
        lastName: userParam.lastName.toLowerCase(),
        email: userParam.email.toLowerCase(),
        password: userParam.password.toLowerCase(),
    };
    console.log('create:');
    console.log(userLowerCase);
    if (await User.findOne({ email: userLowerCase.email })) {
        throw 'Username "' + userLowerCase.email + '" is already taken';
    }

    const user = new User(userLowerCase);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, headers, userParam) {

    //get token
    let token = headers['x-access-token'] || headers['authorization'];
    if (token.startsWith('Bearer ')) {
        //remove bearer, keep only token
        token = token.slice(7, token.length);
    }

    let data;
    // Verify token
    try {
        data = jwt.verify(token, config.secret);
        
    } catch(err) {
        // token invalid
        console.log(err);
        throw 'Invalid token';
    }

    //something is wrong if id in token and id to edit isn't the same
    // I could remove this statement since id is taken from the token, but where's the fun in that...
    if (id != data.sub) throw 'OI, you trying to hack someone? You cheeky wanker';
    //get user by token id
    const user = await User.findById(data.sub); //id is en deoded.sub

    // validate email
    // TODO: log user out of not found
    if (!user) throw 'User not found';
    if (user.email !== userParam.email && await User.findOne({ username: userParam.email })) {
        throw 'Username "' + userParam.email + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);
    
    //save to db
    await user.save();

    //remove hash form user
    console.log(user);

    const { hash, ...userWithoutHash } = user.toObject();

    console.log('updated user:');
    console.log(
        {
        ...userWithoutHash
        }
    );

    return {
        ...userWithoutHash
    };
    //return user;
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}