const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    hash: {type: String, unique: true, required: true},
    firstName: { type: String, lowercase: true, required: true },
    lastName: { type: String, lowercase: true, required: true },
    email: { type: String, lowercase: true, unique: true, required: true },
    type: { type: String },
    phone: { type: String, unique: false },
    abo: { type: String},
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);