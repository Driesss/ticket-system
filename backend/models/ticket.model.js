const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    concertId: {type: Number, required: true},
    userId: {type: Number, required: true, unique: true},
    discription: {type: String, required: true},
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);