const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    bandName: {type: String, lowercase: true, unique: true, required: true},
    bandId: {type: Number, required: true },
    date: { type: Date, required: true },
    duration: { type: Number, required: true },
    createdDate: { type: Date, default: Date.now },
    tickets: { type: Array }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);